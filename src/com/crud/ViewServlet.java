package com.crud;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException 
		{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		out.println("<a href='welcome.html'>Add New User</a>");
		out.println("<center><h1>All Users List</h1></center>");
		List<User> list=UserDao.getAllUser();
		out.print("<table border='1' width='100%;'");
		out.print("<tr><th>Id</th><th>Name</th><th>Password</th><th>Email</th><th>Country</th><th>Edit</th><th>Delete</th></tr>");
		for(User u:list){
			out.print("<tr><td>"+u.getId()+"</td><td>"+u.getName()+"</td><td>"+u.getPassword()+"</td><td>"+u.getEmail()+"</td><td>"+u.getCountry()+"</td><td><a href='UpdateServlet?id="+u.getId()+"'>edit</a></td><td><a href='DeleteServlet?id="+u.getId()+"'>delete</a></td></tr>");
		}
		out.print("</table>");
		out.close();
	}
}