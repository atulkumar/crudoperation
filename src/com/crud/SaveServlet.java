package com.crud;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SaveServlet extends HttpServlet
{
	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		String email=request.getParameter("email");
		String country=request.getParameter("country");
			
		User u=new User();
		u.setName(name);
		u.setPassword(password);
		u.setEmail(email);
		u.setCountry(country);
		int status=UserDao.saveUser(name, password, email, country);
		if(status>0)
		{
			out.print("<p>Record saved successfully!</p>");
			request.getRequestDispatcher("welcome.html").include(request, response);
		}
		else
		{
			out.println("Sorry! unable to save record");
		}
		out.close();
	}
}