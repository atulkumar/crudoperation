package com.crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDao 
{
	public static Connection getConnection()
	{
		Connection con=null;
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","1234");
		}
		catch(Exception e){System.out.println(e);}
		return con;
	}
	
	public static int saveUser(String name,String password,String email,String country)
	{
		String sql2 = "SELECT MAX(ID) AS MAX_ID FROM USER1";
		int id = 0;
		try(Connection con = UserDao.getConnection();
				Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sql2))
		{
			if(rs.next())
		{
				id = rs.getInt("MAX_ID");
		}
			id++;
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		int status = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO USER1 ");
		sql.append("VALUES(");
		sql.append(id+", ");
		sql.append("'"+name+"',");
		sql.append("'"+password+"', ");
		sql.append("'"+email+"', ");
		sql.append("'"+country+"')");
		try(Connection con = UserDao.getConnection();
				Statement stmt = con.createStatement())
		{
			status = stmt.executeUpdate(sql.toString());//executeUpdate required sql
	}
	catch(SQLException ex)
	{
		ex.printStackTrace();
	}
	return status;
	}
	public static int update(User u)
	{
		int status=0;
		try
		{
			Connection con=UserDao.getConnection();
			PreparedStatement ps=con.prepareStatement("update user1 set name=?,password=?,email=?,country=? where id=?");
			ps.setString(1,u.getName());
			ps.setString(2,u.getPassword());
			ps.setString(3,u.getEmail());
			ps.setString(4,u.getCountry());
			ps.setInt(5,u.getId());
			status=ps.executeUpdate();
			con.close();
		}
		catch(Exception ex){ex.printStackTrace();}
		return status;
	}
	public static int delete(int id)
	{
		int status=0;
		try
		{
			Connection con=UserDao.getConnection();
			PreparedStatement ps=con.prepareStatement("delete from user1 where id=?");
			ps.setInt(1,id);
			status=ps.executeUpdate();
			con.close();
		}
		catch(Exception e){e.printStackTrace();}
		return status;
	}
	public static User getUserById(int id)
	{
		User u=new User();
		try{
			Connection con=UserDao.getConnection();
			PreparedStatement ps=con.prepareStatement("select * from user1 where id=?");
			ps.setInt(1,id);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				u.setId(rs.getInt(1));
				u.setName(rs.getString(2));
				u.setPassword(rs.getString(3));
				u.setEmail(rs.getString(4));
				u.setCountry(rs.getString(5));
			}
			con.close();
		}
		catch(Exception ex){ex.printStackTrace();}
		return u;
	}
	public static List<User> getAllUser()
	{
		List<User> list=new ArrayList<User>();
		try
		{
			Connection con=UserDao.getConnection();
			PreparedStatement ps=con.prepareStatement("select * from user1");
			ResultSet rs=ps.executeQuery();
			while(rs.next())
			{
				User u=new User();
				u.setId(rs.getInt(1));
				u.setName(rs.getString(2));
				u.setPassword(rs.getString(3));
				u.setEmail(rs.getString(4));
				u.setCountry(rs.getString(5));
				list.add(u);
			}
			con.close();
		}
		catch(Exception e){e.printStackTrace();}
		return list;
	}
}