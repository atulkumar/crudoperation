package com.crud;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet("/UpdateServlet")//if we used annotation don't required web.xml file
public class UpdateServlet2 extends HttpServlet
{
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String sid=request.getParameter("id");
		int id=Integer.parseInt(sid);
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		String email=request.getParameter("email");
		String country=request.getParameter("country");
		User u=new User();
		u.setId(id);
		u.setName(name);
		u.setPassword(password);
		u.setEmail(email);
		u.setCountry(country);
		int status=UserDao.update(u);
		if(status>0)
		{
			response.sendRedirect("ViewServlet");
		}
		else
		{
			out.println("Sorry! unable to update record");
		}
		out.close();
	}
}